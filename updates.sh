#!/bin/bash
# Checks if any app updates are available and downloads them if there are

set -o noclobber
set -o errexit
set -o nounset

gchange() {
    echo Update gchange...
    UPDATE_URL=https://api.github.com/repos/duniter-gchange/gchange-client/releases/latest
    LATEST_VERSION="$(curl -s $UPDATE_URL | jq -r '.name')"
    echo Latest version is $LATEST_VERSION
    i=0
    for row in $(curl -s $UPDATE_URL | jq -c '.assets[]'); do
        name=$(echo $row | jq '.name')
        if [ ${name: -4} == 'apk"' ]; then
            echo .apk file, use for update
            wget -O fdroid/repo/gchange.apk "$(curl -s $UPDATE_URL | jq -r ".assets[$i].browser_download_url")"
            sed -i "s/CurrentVersion:.*/CurrentVersion: $LATEST_VERSION /" fdroid/metadata/fr.gchange.yml
        else
        echo Not .apk file, skip $name
        fi
        i=$i+1
    done
}
cesium() {
    UPDATE_URL=https://api.github.com/repos/duniter/cesium/releases/latest
    LATEST_VERSION="$(curl -s $UPDATE_URL | jq -r '.name')"
    echo Latest version is $LATEST_VERSION
    i=0
    for row in $(curl -s $UPDATE_URL | jq -c '.assets[]'); do
        name=$(echo $row | jq '.name')
        if [ ${name: -4} == 'apk"' ]; then
            wget -O fdroid/repo/cesium.apk "$(curl -s $UPDATE_URL | jq -r ".assets[$i].browser_download_url")"
            sed -i "s/CurrentVersion:.*/CurrentVersion: $LATEST_VERSION /" fdroid/metadata/fr.duniter.cesium.yml
        else
        echo Not .apk file, skip $name
        fi
        i=$i+1
    done
}
# Check for updates for applications listed below
cesium
gchange
